from __future__ import annotations

from sqlalchemy.ext.asyncio import AsyncSession

from reposchau.database import async_session

from typing import Annotated
from fastapi import Depends


async def get_database_session():
    async with async_session() as session:
        yield session


DbSession = Annotated[AsyncSession, Depends(get_database_session)]
