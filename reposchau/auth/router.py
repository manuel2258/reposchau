from fastapi import APIRouter
from reposchau.auth.schema import UserCreate, UserRead, UserUpdate


from reposchau.auth.users import users, auth_backend

router = APIRouter(prefix="/auth", tags=["auth"])

router.include_router(
    users.get_auth_router(auth_backend, requires_verification=True),
)

router.include_router(
    users.get_register_router(UserRead, UserCreate),
)

router.include_router(
    users.get_verify_router(UserRead),
)

router.include_router(
    users.get_reset_password_router(),
)

router.include_router(
    users.get_users_router(UserRead, UserUpdate, requires_verification=True),
)
