from reposchau.auth.users import users
from reposchau.auth.model import User

from typing import Annotated
from fastapi import Depends


current_active_user = users.current_user(active=True)

CurrentUser = Annotated[User, Depends(current_active_user)]
