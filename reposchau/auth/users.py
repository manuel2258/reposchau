import logging
import uuid
from typing import Annotated, Optional

from fastapi import Depends, Request
from fastapi_users import BaseUserManager, FastAPIUsers, UUIDIDMixin
from fastapi_users.authentication import AuthenticationBackend, BearerTransport
from fastapi_users.authentication.strategy import DatabaseStrategy
from fastapi_users.db import SQLAlchemyUserDatabase
from fastapi_users_db_sqlalchemy.access_token import SQLAlchemyAccessTokenDatabase
from sqlalchemy.ext.asyncio import AsyncSession

from reposchau.auth.model import AccessToken, User
from reposchau.dependency import DbSession, get_database_session
from reposchau.project.model import Namespace

log = logging.getLogger(__name__)

# Replace me!
SECRET = "SECRET"


async def get_user_db(session: DbSession):
    yield SQLAlchemyUserDatabase(session, User)


UserDatabase = Annotated[SQLAlchemyUserDatabase, Depends(get_user_db)]


async def get_access_token_db(session: DbSession):
    yield SQLAlchemyAccessTokenDatabase(session, AccessToken)


AccessTokenDatabase = Annotated[
    SQLAlchemyAccessTokenDatabase, Depends(get_access_token_db)
]


bearer_transport = BearerTransport(tokenUrl="auth/login")


def get_token_database_stategy(access_token_db: AccessTokenDatabase):
    return DatabaseStrategy(access_token_db, lifetime_seconds=3600)


auth_backend = AuthenticationBackend(
    name="db",
    transport=bearer_transport,
    get_strategy=get_token_database_stategy,
)


class UserManager(UUIDIDMixin, BaseUserManager[User, uuid.UUID]):
    reset_password_token_secret = SECRET
    verification_token_secret = SECRET

    async def on_after_register(self, user: User, request: Optional[Request] = None):
        user_namespace = Namespace(name=user.username, owners=[user])
        session: AsyncSession = self.user_db.session
        session.add(user_namespace)
        await session.commit()
        log.info(
            f"User {user.id} has registered, created default namespace {user_namespace}"
        )

    async def on_after_forgot_password(
        self, user: User, token: str, request: Optional[Request] = None
    ):
        log.info(f"User {user.id} has forgot their password. Reset token: {token}")

    async def on_after_request_verify(
        self, user: User, token: str, request: Optional[Request] = None
    ):
        log.info(
            f"Verification requested for user {user.id}. Verification token: {token}"
        )


async def get_user_manager(user_db: UserDatabase):
    yield UserManager(user_db)


users = FastAPIUsers[User, uuid.UUID](
    get_user_manager,
    [auth_backend],
)
