from sqlalchemy.orm import Mapped
from reposchau.database import Base
from fastapi_users.db import SQLAlchemyBaseUserTableUUID
from fastapi_users_db_sqlalchemy.access_token import SQLAlchemyBaseAccessTokenTableUUID


class User(SQLAlchemyBaseUserTableUUID, Base):
    username: Mapped[str]


class AccessToken(SQLAlchemyBaseAccessTokenTableUUID, Base):
    pass
