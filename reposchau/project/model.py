from typing import List
from sqlalchemy import Column, Table
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.orm.properties import ForeignKey
from reposchau.auth.model import User
from reposchau.database import Base


user_namespace_table = Table(
    "user_namespace",
    Base.metadata,
    Column("user", ForeignKey("user.id")),
    Column("namespace", ForeignKey("namespace.id")),
)


class Namespace(Base):
    __tablename__ = "namespace"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(unique=True)
    owners: Mapped[List[User]] = relationship(secondary=user_namespace_table)
