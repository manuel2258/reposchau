from fastapi import FastAPI
from logging import config
import uvicorn
from reposchau.auth import CurrentUser
from reposchau.auth.router import router as auth_router
from reposchau.database import Base, engine

from reposchau.dependency import DbSession

config.fileConfig("logging.conf", disable_existing_loggers=False)

app = FastAPI()

app.include_router(auth_router)


@app.get("/")
async def root(db: DbSession, user: CurrentUser):
    print(user)
    return {"message": "Hello World"}


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)


def serve():
    uvicorn.run(app, host="0.0.0.0", port=8000, log_level="info")


if __name__ == "__main__":
    serve()
