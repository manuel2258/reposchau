from sqlalchemy.orm import declarative_base
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine

SQLALCHEMY_DATABASE_URL = "sqlite+aiosqlite:///./local_dev.db"

engine = create_async_engine(
    url=SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
async_session = async_sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
