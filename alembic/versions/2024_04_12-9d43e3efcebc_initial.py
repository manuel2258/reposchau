"""initial

Revision ID: 9d43e3efcebc
Revises: 
Create Date: 2024-04-12 12:19:34.151156+00:00

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from reposchau.database import Base


# revision identifiers, used by Alembic.
revision: str = "9d43e3efcebc"
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
